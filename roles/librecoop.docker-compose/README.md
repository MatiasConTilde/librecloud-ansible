Docker-compose Role
===================

This roles deploys a Librecoops cloud that consists of:

- traefik
- backup-bot-two
- odoo
- nextcloud
- onlyoffice
- mailu
- wordpress
- vaultwarden
- seafile
- grafana

Requirements
------------

TBD

For onlyoffice in nextcloud, after deploying:
- install the ONLYOFFICE app
- in the onlyoffice settings:
  - set `ONLYOFFICE Docs address` to `https://{{ onlyoffice_host }}/`
  - set `Secret key` to `{{ onlyoffice_secret_key }}`

After deploying seafile you might want to change a few options:

- in `seafile.conf`
```
[fileserver]
max_sync_file_count = -1
fs_id_list_request_timeout = -1
```
```
[notification]
enabled = true
```
```
[file_lock]
default_expire_hours = 2
use_locked_file_cache = true
```

- in `seafdav.conf`
```
[WEBDAV]
enabled = true
```

- in `seafevents.conf` (only in pro version)
```
[INDEX FILES]
external_es_server = true
es_host = elasticsearch
es_port = 9200
scheme = http
username = elastic
password = ********************
```
docker compose exec elasticsearch /usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic

- in `seahub_settings.py` (if onlyoffice enabled)
```
SERVICE_URL = "https://{{ seafile_host }}/"
```
```
ENABLE_ONLYOFFICE = True
ONLYOFFICE_JWT_SECRET = '********************************'
VERIFY_ONLYOFFICE_CERTIFICATE = True
ONLYOFFICE_APIJS_URL = 'https://{{ onlyoffice_host }}/web-apps/apps/api/documents/api.js'
ONLYOFFICE_FILE_EXTENSION = ('doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'odt', 'fodt', 'odp', 'fodp', 'ods', 'fods')
ONLYOFFICE_EDIT_FILE_EXTENSION = ('docx', 'pptx', 'xlsx')
ONLYOFFICE_FORCE_SAVE = True

EMAIL_USE_TLS = True
EMAIL_HOST = '{{ mailu_host }}'
EMAIL_HOST_USER = 'admin@{{ mailu_domain }}'
EMAIL_HOST_PASSWORD = '{{ mailu_admin_password }}'
EMAIL_PORT = 465
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = EMAIL_HOST_USER
```

Role Variables
--------------

## General

### acme_email

The email to be used with letsencrypt.

### as_root

Execute all steps as root, otherwise as the user who is connecting with ssh. Default: `True`.

### clear

If set to `True` or a dictionary, clean up workloads (`docker compose down`). If a dictionary, possible keys: `images` (default `False`), `volumes` (default `False`), `build_cache` (default `True`), `networks` (default `True`). The keys define if it should prune it additionaly. Default: `False`.

### deploy

If set to `True`, the role will deploy the cloud. Default: `True`.

### development

If set to `True`, the role will make some shortcuts that might only be acceptable in development environments. Default: `False`.

### enable

A list to select which services to deploy. Possible vaules: `'odoo'`, `'nextcloud'`, `'onlyoffice'`, `'mailu'`, `'wordpress'`, `'vaultwarden'`, `'seafile'`, `'grafana'`. Default: `[]`.

### enable_backup

If set to `True` the backubot container will be deploy to make periodic backups of deployed containers. Default: `True`.

### make_backup

If set to `True`, the role will make a backup with backup-bot-two. Default: `False`.

### reset

If set to `True`, the role will remove all running containers and images before deploying. Default: `False`.

### workload_src_path

Path to store the docker-compose and other files. Default: `/src`.

## Backup configuration

### backup_restic_host

URL of the S3 bucket.

### backup_restic_name

Name of the restic repository. Default is `{{ inventory_hostname }}`.

### backup_restic_password

The password to be used to encrypt the restic repository.

### backup_aws_access_key_id

The ID of the S3 account. `AWS_ACCESS_KEY_ID`.

### backup_aws_secret_access_key

The key of the S3 account. `AWS_SECRET_ACCESS_KEY`.

## Odoo configuration

### odoo_image

The Odoo image to deploy. Obviously there might be some assumptions that are made about the behavior of Odoo's
image. This means that that not all images will work, but there's still a chance you want to modify this variable
to select a specific version.

### odoo_host

The hostname used to access Odoo. By default `odoo.{{ inventory_hostname }}` will be used.

### odoo_db_user

The user that Odoo shall use to connect to the database.

### odoo_db_name

The database name that Odoo shall use.

### odoo_db_password

The password for the database.

### odoo_master_password

The master password for the database.

## Nextcloud configuration

### nextcloud_host

The hostname used to access Nextcloud. By default `cloud.{{ inventory_hostname }}` will be used.

### nextcloud_db_user

The user that nextcloud shall use to connect to the database.

### nextcloud_db_name

The database name that nextcloud shall use.

### nextcloud_db_password

The password for the database.

### nextcloud_admin_password

The password for the admin account.

### nextcloud_secret_key

The secret key for nextcloud.

## Onlyoffice

### onlyoffice_host

The hostname used to access Onlyoffice. By default `onlyoffice.{{ inventory_hostname }}` will be used.

### onlyoffice_secret_key

The secret key for onlyoffice.

## Mailu configuration

### mailu_domain

Main mail domain.

### mailu_host

The hostname used to access mailu. By default `mail.{{ inventory_hostname }}` will be used.

### mailu_enable

A list to select which extra services to deploy. Possible vaules: `'webdav'`, `'oletools'`, `'fetchmail'`, `'clamav'`. Default: `[]`

### mailu_secret_key

Set to a randomly generated 16 bytes string.

### mailu_admin_password

The password for the admin@{{ mailu_domain }} account.

## Wordpress configuration

### wordpress_host

The hostname used to access wordpress. By default `{{ inventory_hostname }}` will be used.

### wordpress_db_user

The user that wordpress shall use to connect to the database.

### wordpress_db_name

The database name that wordpress shall use.

### wordpress_db_password

The password for the database.

## Vaultwarden configuration

### vaultwarden_host

The hostname used to access vaultwarden. By default `vault.{{ inventory_hostname }}` will be used.

### vaultwarden_admin_token

The admin token for vaultwarden.

## Seafile configuration

### seafile_host

The hostname used to access seafile. By default `seafile.{{ inventory_hostname }}` will be used.

### seafile_db_password

The password for the database root user, which will be used the first time to create the other database users.

### seafile_admin_email

The email for the admin account. By default `admin@{{ inventory_hostname }}` will be used.

### seafile_admin_password

The password for the admin account.

### seafile_pro

Whether to use community or pro version. Default: `False`

If using pro version, you will need to create an account at https://customer.seafile.com/ and follow the instructions to login to the docker registry.

### seafile_enable

A list to select which extra services to deploy. Possible vaules: `'elasticsearch'`. Default: `[]`

### seafile_elasticsearch_memory_gb

How many GB to allocate to the elasticsearch

## Grafana configuration

### grafana_host

The hostname used to access grafana By default `grafana.{{ inventory_hostname }}` will be used

### loki_host

The hostname used to access loki By default `loki.{{ inventory_hostname }}` will be used

### monitoring_admin_password_hash

The hashed password used to interact with the various monitoring services

### monitoring_admin_password

The clear password used to interact with the various monitoring services

### prometheus_targets

Array of url to pool monitoring data from. By default ['node-exporter:9100', 'cadvisor:8080']

## Monitoring agent configuration

### node_exporter_host

The hostname used to access node_exporter. By default `nodeexporter.{{ inventory_hostname }}` will be used

### cadvisor_host

The hostname used to access Cadvisor. By default `cadvisor.{{ inventory_hostname }}` will be used

### monitoring_admin_password_hash

The password to setup basic auth for both node_exporter and cadvisor.

### monitoring_admin_password

The clear password used to interact with the various monitoring services

## Extra workloads

It is possible to define workloads that are not included by default, they follow this scheme:
```yaml
extra_workloads:
  - name: name
    repo: https://giturl.com/repo.git
    environment:
      ENV_VARIABLE: value
```

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: librecoop.docker-compose
      vars:
        odoo_host: "odoo.librecoop.es"
```

License
-------

GPLv3

Author Information
------------------

librecoop https://www.librecoop.es
