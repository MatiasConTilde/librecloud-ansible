# Input argument
#
# Each element in the docker_compose_workload list can contain the following keys:
#
# - name: Name of the workload
#
# - files: Dictionary of files to be processed
#
# - files.root: The root of the files, if defined it will be copied entirely
#
# - files.templates: List of template files to be processed. They are searched inside files.root if it is defined, and in the default templates directory otherwise
#
# - environment: Dictionary of ENV variables
#
# - repo: If provided, git repository to get the docker-compose from (incompatible with `templates`
#
# - version : If provided, git branch to get checkout (incompatible with `templates`
#
# - restart: If set to True, force a restar the docker-compose even if there are no changes.
#
# - remove_volumes: If set to True, remove all volumes before restarting the compose (implies `restart`)

- name: Deploy
  block:

  - name: Git clone
    git:
      repo: "{{ item.repo }}"
      dest: "{{ workload_src_path }}/{{ item.name }}"
      version: "{{ item.version | default('main') }}"
      accept_hostkey: true
    loop: "{{ docker_compose_workload }}"
    when: item.repo | default(false)

  - name: Template
    block:
      - name: Empty directory
        file:
          path: "{{ workload_src_path }}/{{ item.name }}"
          state: absent
        loop: "{{ docker_compose_workload }}"

      - name: Create directory
        file:
          path: "{{ workload_src_path }}/{{ item.name }}"
          state: directory
        loop: "{{ docker_compose_workload }}"

      - name: Copy files
        synchronize:
          src: "{{ item.files.root }}/{{ item.name }}"
          dest: "{{ workload_src_path }}/"
        when: item.files.root | default(false)
        loop: "{{ docker_compose_workload }}"

      - name: Template workload
        include_tasks:
          file: template.yml
        vars:
          name: "{{ item.name }}"
          files: "{{ item.files }}"
        when: item.files.templates | default(false)
        loop: "{{ docker_compose_workload }}"

    when: item.files | default(false)

  - name: Tear down
    community.docker.docker_compose:
      project_src: "{{ workload_src_path }}/{{ item.name }}/{{ item.subdir | default() }}"
      remove_volumes: "{{item.remove_volumes}}"
      state: absent
    environment: "{{ item.environment | default({}) }}"
    loop: "{{ docker_compose_workload }}"
    when: item.restart | default(false) or item.remove_volumes | default(false)
    register: teardown
    until: not teardown.changed
    retries: 5
    delay: 1

  - name: Up
    community.docker.docker_compose:
      project_src: "{{ workload_src_path }}/{{ item.name }}/{{ item.subdir | default() }}"
      project_name: "{{ item.name }}"
      pull: true
      remove_orphans: true
    environment: "{{ item.environment | default({}) }}"
    loop: "{{ docker_compose_workload }}"

  when: item.when | default(true)
