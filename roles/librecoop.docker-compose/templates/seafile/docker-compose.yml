version: '3'

services:
  seafile:
{% if not seafile_pro %}
    image: seafileltd/seafile-mc:{{ seafile_image_version }}
{% else %}
    image: docker.seadrive.org/seafileltd/seafile-pro-mc:{{ seafile_pro_image_version }}
{% endif %}
    volumes:
      - seafile-data:/shared/seafile
    environment:
      - DB_HOST=db
      - DB_ROOT_PASSWD=${DB_PASSWORD}
      - TIME_ZONE=Europe/Madrid
      - SEAFILE_ADMIN_EMAIL={{ seafile_admin_email }}
      - SEAFILE_ADMIN_PASSWORD=${ADMIN_PASSWORD}
      - SEAFILE_SERVER_LETSENCRYPT=false
      - SEAFILE_SERVER_HOSTNAME={{ seafile_host }}
      - FORCE_HTTPS_IN_CONF=true
    depends_on:
      - db
      - memcached
{% if seafile_pro %}
      - elasticsearch
{% endif %}

    labels:
      traefik.enable: "true"

      traefik.http.middlewares.seafile-https.redirectscheme.scheme: "https"

      traefik.http.routers.seafile-http.entrypoints: "web"
      traefik.http.routers.seafile-http.rule: "Host(`{{ seafile_host }}`)"
      traefik.http.routers.seafile-http.middlewares: "seafile-https@docker"

      traefik.http.routers.seafile.entrypoints: "websecure"
      traefik.http.routers.seafile.rule: "Host(`{{ seafile_host }}`)"

      traefik.http.routers.seafile.tls: "true"
      traefik.http.routers.seafile.tls.certresolver: "default"

      backupbot.backup: "true"
      backupbot.backup.volumes: "true"
{% if 'monitoring-agent' in enable or 'grafana' in enable | default(false) %}
    logging:
      driver: loki:latest
      options:
      {% if development %}
        loki-url: "http://admin:${MONITORING_PASSWORD}@{{loki_host}}/loki/api/v1/push"
      {% else %}
        loki-url: "https://admin:${MONITORING_PASSWORD}@{{loki_host}}/loki/api/v1/push"
      {% endif %}
{% endif %}

  db:
    image: mariadb:{{ mariadb_image_version }}
    environment:
      - MYSQL_ROOT_PASSWORD=${DB_PASSWORD}
      - MYSQL_LOG_CONSOLE=true
    volumes:
      - seafile-db-data:/var/lib/mysql
    labels:
      backupbot.backup: "true"
      backupbot.backup.pre-hook: 'mariadb-dump -uroot -p"${DB_PASSWORD}" --opt ccnet_db -r /tmp/ccnet.sql; mariadb-dump -uroot -p"${DB_PASSWORD}" --opt seafile_db -r /tmp/seafile.sql; mariadb-dump -uroot -p"${DB_PASSWORD}" --opt seahub_db -r /tmp/seahub.sql'
      backupbot.backup.path: "/tmp/ccnet.sql,/tmp/seafile.sql,/tmp/seahub.sql"
      backupbot.backup.post-hook: "rm -rf /tmp/ccnet.sql /tmp/seafile.sql /tmp/seahub.sql"

  memcached:
    image: memcached:{{ memcached_image_version }}
    container_name: seafile-memcached
    entrypoint: memcached -m 256

{% if seafile_pro and 'elasticsearch' in seafile_enable %}
  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:{{ elasticsearch_image_version }}
    restart: on-failure
    environment:
      - discovery.type=single-node
      - bootstrap.memory_lock=true
      - xpack.security.http.ssl.enabled=false
    mem_limit: {{ seafile_elasticsearch_memory_gb }}G
    volumes:
      - seafile-elasticsearch-data:/usr/share/elasticsearch/data
{% endif %}

volumes:
  seafile-data:
  seafile-db-data:
{% if seafile_pro %}
  seafile-elasticsearch-data:
{% endif %}
